-- phpMyAdmin SQL Dump
-- version 5.0.2
-- https://www.phpmyadmin.net/
--
-- Hôte : 127.0.0.1:3306
-- Généré le : Dim 30 mai 2021 à 20:21
-- Version du serveur :  5.7.31
-- Version de PHP : 7.3.21

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de données : `marketplace`
--

-- --------------------------------------------------------

--
-- Structure de la table `acheter`
--

DROP TABLE IF EXISTS `acheter`;
CREATE TABLE IF NOT EXISTS `acheter` (
  `IDArticle` int(11) NOT NULL,
  `CodeClient` int(11) NOT NULL,
  PRIMARY KEY (`IDArticle`,`CodeClient`),
  KEY `acheter_Client0_FK` (`CodeClient`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Structure de la table `administrateur`
--

DROP TABLE IF EXISTS `administrateur`;
CREATE TABLE IF NOT EXISTS `administrateur` (
  `IDAdministrateur` int(11) NOT NULL AUTO_INCREMENT,
  `Nom` varchar(50) NOT NULL,
  `Prenom` varchar(50) NOT NULL,
  `E_mail` varchar(50) NOT NULL,
  `Mot_de_Passe` varchar(50) NOT NULL,
  PRIMARY KEY (`IDAdministrateur`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

--
-- Déchargement des données de la table `administrateur`
--

INSERT INTO `administrateur` (`IDAdministrateur`, `Nom`, `Prenom`, `E_mail`, `Mot_de_Passe`) VALUES
(1, 'seb', 'Admin1', 'admin1@email.fr', 'admin1'),
(2, 'wal', 'Admin2', 'admin2@email.fr', 'admin2');

-- --------------------------------------------------------

--
-- Structure de la table `ajouter`
--

DROP TABLE IF EXISTS `ajouter`;
CREATE TABLE IF NOT EXISTS `ajouter` (
  `CodeVendeur` int(11) NOT NULL,
  `IDAdministrateur` int(11) NOT NULL,
  PRIMARY KEY (`CodeVendeur`,`IDAdministrateur`),
  KEY `ajouter_Administrateur0_FK` (`IDAdministrateur`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Structure de la table `article`
--

DROP TABLE IF EXISTS `article`;
CREATE TABLE IF NOT EXISTS `article` (
  `IDArticle` int(11) NOT NULL AUTO_INCREMENT,
  `CodeVendeur` int(100) NOT NULL,
  `type_achat` varchar(155) NOT NULL,
  `Nom` varchar(255) NOT NULL,
  `Descriptions` varchar(255) NOT NULL,
  `Categorie` varchar(50) NOT NULL,
  `Prix` int(10) NOT NULL,
  `Image1` varchar(2024) NOT NULL,
  `Image2` varchar(2024) NOT NULL,
  `Image3` varchar(2024) NOT NULL,
  PRIMARY KEY (`IDArticle`)
) ENGINE=InnoDB AUTO_INCREMENT=40 DEFAULT CHARSET=latin1;

--
-- Déchargement des données de la table `article`
--

INSERT INTO `article` (`IDArticle`, `CodeVendeur`, `type_achat`, `Nom`, `Descriptions`, `Categorie`, `Prix`, `Image1`, `Image2`, `Image3`) VALUES
(28, 1, 'vente', 'Macbook Pro 13', 'Je vends un MacBook Pro ', 'Technologie', 1200, 'https://store.storeimages.cdn-apple.com/4668/as-images.apple.com/is/mbp-spacegray-select-202011_GEO_EMEA_LANG_FR?wid=904&hei=840&fmt=jpeg&qlt=80&.v=1613672865000', '', ''),
(33, 8, 'vente', 'Iphone', 'Iphone en parfait etat', 'Technologie', 600, 'https://media.ldlc.com/r1600/ld/products/00/04/65/15/LD0004651540_2.jpg', '', ''),
(34, 8, 'enchere', 'Table', 'vend table pas chere en tres bon etat ', 'meubles', 200, 'https://cdn.laredoute.com/products/680by680/7/0/3/703e9c8c1cc59d91fa0ff8e48258770e.jpg', '', ''),
(35, 8, 'vente', 'Tableau', 'Tableau en bon etat,recuperer en main propre', 'meubles', 100, 'https://www.milome.fr/11165/tableau-moderne-grace-60x80-cm.jpg', '', ''),
(36, 1, 'enchere', 'Maillot', 'Maillot en parfait etat', 'vetements', 95, 'https://footdealer.com/wp-content/uploads/2021/05/MAILLOT-EQUIPE-DE-FRANCE-DOMICILE-EURO-2021-BENZEMA-1.jpg', '', ''),
(37, 1, 'vente', 'Ecran Predator 27 pouces', 'Ecran comme neuf', 'technologies', 250, 'https://images-na.ssl-images-amazon.com/images/I/618i4hfbMLL._AC_SX425_.jpg', '', '');

-- --------------------------------------------------------

--
-- Structure de la table `carte_bancaire`
--

DROP TABLE IF EXISTS `carte_bancaire`;
CREATE TABLE IF NOT EXISTS `carte_bancaire` (
  `ID` int(100) NOT NULL AUTO_INCREMENT,
  `CodeClient` int(100) NOT NULL,
  `Type_carte` varchar(255) NOT NULL,
  `Numero` varchar(17) NOT NULL,
  `Cvv` int(3) NOT NULL,
  `Dates` date NOT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=MyISAM AUTO_INCREMENT=23 DEFAULT CHARSET=latin1;

--
-- Déchargement des données de la table `carte_bancaire`
--

INSERT INTO `carte_bancaire` (`ID`, `CodeClient`, `Type_carte`, `Numero`, `Cvv`, `Dates`) VALUES
(21, 21, 'visa', '1234567892222222', 789, '2021-05-26'),
(19, 29, 'visa', '1234567891111111', 123, '2021-05-04'),
(22, 28, 'visa', '5555555555555555', 123, '2021-05-18');

-- --------------------------------------------------------

--
-- Structure de la table `client`
--

DROP TABLE IF EXISTS `client`;
CREATE TABLE IF NOT EXISTS `client` (
  `CodeClient` int(11) NOT NULL AUTO_INCREMENT,
  `Nom` varchar(50) NOT NULL,
  `Prenom` varchar(50) NOT NULL,
  `Email` varchar(50) NOT NULL,
  `Mot_de_passe` varchar(255) NOT NULL,
  `Adresse_ligne_1` varchar(255) NOT NULL,
  `Pays` varchar(255) NOT NULL,
  `Code_postal` varchar(255) NOT NULL,
  `Ville` varchar(255) NOT NULL,
  `Numero_de_telephone` varchar(255) NOT NULL,
  PRIMARY KEY (`CodeClient`)
) ENGINE=InnoDB AUTO_INCREMENT=30 DEFAULT CHARSET=latin1;

--
-- Déchargement des données de la table `client`
--

INSERT INTO `client` (`CodeClient`, `Nom`, `Prenom`, `Email`, `Mot_de_passe`, `Adresse_ligne_1`, `Pays`, `Code_postal`, `Ville`, `Numero_de_telephone`) VALUES
(21, 'Waligora', 'Paul', 'test@email.fr', '$2y$10$yrrAaHx4vvibDui7Im1VFexVuFUfkeR286X1GMYEUsVeavdbUfIZq', 'rue de la liberte', 'France', '75014', 'Paris', '0784659231'),
(28, 'Sebire', 'Alexandre', 'a.sebire@outlook.fr', '$2y$10$BnBmBP935/JjN9zX.C4d1ObgoCk4oAEGYDZi9rw0hysbVj19FLxXq', '61 rue pascal', 'France', '75013', 'Paris', '0633162823'),
(29, 'ali', 'Mohamed', 'test@gmail.com', '$2y$10$C.XtNRddh3kZHbO.5VNkZ.tMPEAj.QsytWZw1uJn72/vyPQvIxzqC', '50 rue du couscouse', 'Algerie', '100', '654', '0231459852');

-- --------------------------------------------------------

--
-- Structure de la table `messages`
--

DROP TABLE IF EXISTS `messages`;
CREATE TABLE IF NOT EXISTS `messages` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `CodeClient` int(11) NOT NULL,
  `CodeVendeur` int(11) NOT NULL,
  `Mess` text NOT NULL,
  `pv` tinyint(1) NOT NULL,
  `pa` tinyint(1) NOT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=MyISAM AUTO_INCREMENT=255 DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Structure de la table `mettre_en_ligne`
--

DROP TABLE IF EXISTS `mettre_en_ligne`;
CREATE TABLE IF NOT EXISTS `mettre_en_ligne` (
  `IDArticle` int(11) NOT NULL,
  `CodeVendeur` int(11) NOT NULL,
  `Prix` int(11) NOT NULL,
  `facon` varchar(50) NOT NULL,
  PRIMARY KEY (`IDArticle`,`CodeVendeur`),
  KEY `mettre_en_ligne_Vendeur0_FK` (`CodeVendeur`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Structure de la table `panier`
--

DROP TABLE IF EXISTS `panier`;
CREATE TABLE IF NOT EXISTS `panier` (
  `ID_panier` int(11) NOT NULL AUTO_INCREMENT,
  `Article` int(11) NOT NULL,
  `CodeClient` int(11) NOT NULL,
  PRIMARY KEY (`ID_panier`)
) ENGINE=MyISAM AUTO_INCREMENT=19 DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Structure de la table `supprimer`
--

DROP TABLE IF EXISTS `supprimer`;
CREATE TABLE IF NOT EXISTS `supprimer` (
  `CodeVendeur` int(11) NOT NULL,
  `IDAdministrateur` int(11) NOT NULL,
  PRIMARY KEY (`CodeVendeur`,`IDAdministrateur`),
  KEY `supprimer_Administrateur0_FK` (`IDAdministrateur`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Structure de la table `vendeur`
--

DROP TABLE IF EXISTS `vendeur`;
CREATE TABLE IF NOT EXISTS `vendeur` (
  `CodeVendeur` int(11) NOT NULL AUTO_INCREMENT,
  `Nom` varchar(255) NOT NULL,
  `Prenom` varchar(255) NOT NULL,
  `Email` varchar(255) NOT NULL,
  `Mot_de_passe` varchar(255) NOT NULL,
  `Adresse_ligne_1` varchar(255) NOT NULL,
  `Pays` varchar(255) NOT NULL,
  `Code_postal` varchar(255) NOT NULL,
  `Ville` varchar(255) NOT NULL,
  `Numero_de_telephone` varchar(255) NOT NULL,
  `Validation` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`CodeVendeur`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=latin1;

--
-- Déchargement des données de la table `vendeur`
--

INSERT INTO `vendeur` (`CodeVendeur`, `Nom`, `Prenom`, `Email`, `Mot_de_passe`, `Adresse_ligne_1`, `Pays`, `Code_postal`, `Ville`, `Numero_de_telephone`, `Validation`) VALUES
(2, 'Waligora', 'Paul', 'paul.waligora@edu.ece.fr', '$2y$10$wlK0ncAZeMz5WeJWzuJ5k.34./Y80DQET67muBVKYiSlwZ6qg8yy2', '172 rue alexia', 'France', '75014', 'Paris', '0998785555', 0),
(8, 'Sebire', 'Alexandre', 'alexandre.sebire@edu.ece.fr', '$2y$10$wlK0ncAZeMz5WeJWzuJ5k.34./Y80DQET67muBVKYiSlwZ6qg8yy2', '61 rue pascal', 'France', '75013', 'Paris', '0633162823', 0),
(9, 'Sebire', 'Jean', 'jean@gmail.com', '$2y$10$bElrL0NOQRCxq6tTPall2e.vmdKrZk.uONjJE5eDk5CED1mFSMlRm', '61 rue de la biere', 'France', '75005', 'Paris', '0689745623', 0);

--
-- Contraintes pour les tables déchargées
--

--
-- Contraintes pour la table `acheter`
--
ALTER TABLE `acheter`
  ADD CONSTRAINT `acheter_Article_FK` FOREIGN KEY (`IDArticle`) REFERENCES `article` (`IDArticle`),
  ADD CONSTRAINT `acheter_Client0_FK` FOREIGN KEY (`CodeClient`) REFERENCES `client` (`CodeClient`);

--
-- Contraintes pour la table `ajouter`
--
ALTER TABLE `ajouter`
  ADD CONSTRAINT `ajouter_Administrateur0_FK` FOREIGN KEY (`IDAdministrateur`) REFERENCES `administrateur` (`IDAdministrateur`),
  ADD CONSTRAINT `ajouter_Vendeur_FK` FOREIGN KEY (`CodeVendeur`) REFERENCES `vendeur` (`CodeVendeur`);

--
-- Contraintes pour la table `mettre_en_ligne`
--
ALTER TABLE `mettre_en_ligne`
  ADD CONSTRAINT `mettre_en_ligne_Article_FK` FOREIGN KEY (`IDArticle`) REFERENCES `article` (`IDArticle`),
  ADD CONSTRAINT `mettre_en_ligne_Vendeur0_FK` FOREIGN KEY (`CodeVendeur`) REFERENCES `vendeur` (`CodeVendeur`);

--
-- Contraintes pour la table `supprimer`
--
ALTER TABLE `supprimer`
  ADD CONSTRAINT `supprimer_Administrateur0_FK` FOREIGN KEY (`IDAdministrateur`) REFERENCES `administrateur` (`IDAdministrateur`),
  ADD CONSTRAINT `supprimer_Vendeur_FK` FOREIGN KEY (`CodeVendeur`) REFERENCES `vendeur` (`CodeVendeur`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
