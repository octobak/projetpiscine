<?php
  session_start();
  if ($_SESSION["CodeClient"]){
?>

<!DOCTYPE html>
<html>

<head>
  <meta charset="utf-8" />
  <html lang="fr">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <link rel="stylesheet" type="text/css" href="../../css/profilA.css">
  <link rel="preconnect" href="https://fonts.gstatic.com">
<link href="https://fonts.googleapis.com/css2?family=Mulish:wght@500&display=swap" rel="stylesheet">
<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>

</head>

<body>
  
  <div id="englobe">

    <!-- navBar -->
    <nav class="navBar">
      <ul>

        <li><a class="active" href="#home">ECE<br>MARKETPLACE</a></li>

        <li>
          <div class="bloc">
            <a href="accueilC.php">
              <p>Accueil</p>
            </a>
          </div>
        </li>

        <li>
          <div class="bloc1">
            <a href="parcourir.php">Parcourir</a>
          </div>
        </li>


        <li>
          <div class="bloc2">
            <a href="notificationA.php">Notifications</a>
          </div>
        </li>

        <li>
            <div class="idBonjour">
              <a> <span>Bonjour</span> <?php echo $_SESSION['Prenom'] ?></a>
            </div>
          </li>


        <li style="float:right" class="nav-item dropdown">
          <a href="panier.php">
            <button class="cad">
              <img src="../../images/caddie.png" class="rounded-circle" height="40" alt="caddie" loading="lazy" />
            </button>
          </a>
        </li>

        <li style="float:right" class="nav-item dropdown">
          <div class="dropdown">
            <button class="dropbtn">
            <img src="../../images/profil.png" class="rounded-circle" height="40" alt="profil" loading="lazy" />
            </button>
            <div class="dropdown-content">
              <a href="profilA.php">Profil</a>
              <a href="../server/deconnexion.php">Déconnexion</a>
            </div>
          </div>
        </li>
      </ul>
    </nav>
   <!-- contenu -->
  
    <div class="gauche">

      
      <h2 id="title">Informations Personnelles</h2>
      
      
      <div class="pprofil">
        <img src="../../images/profil.png" class="rounded-circle" height="180" alt="profilP" loading="lazy" />
      </div>

      <?php

      $cc = $_SESSION["CodeClient"];
      $messErreur="";
      $database = "marketPlace";
      $db_handle = mysqli_connect('localhost','root','');
      $db_found = mysqli_select_db($db_handle, $database);
     
      if ($db_found) {
        $sql = "SELECT * FROM carte_bancaire WHERE CodeClient LIKE $cc";
              $result = mysqli_query($db_handle, $sql);
             
              $tpCarte = '';
              $num = '';
              $date = '';
              $cvv = '';

              if(mysqli_num_rows($result) == 0){
                $messErreur="Carte ajouté avec succès";
              }else{
                $messErreur="Vous possedez une carte, supprimer la carte existante pour en ajouter une nouvelle";  
                          }
              while ($data = mysqli_fetch_assoc($result)) {
                $tpCarte = $data['Type_carte'];
                $num = $data['Numero'];
                $date = $data['Dates'];
                $cvv = $data['Cvv'];
              }
            }
      ?>
      
      <div class="liste">
        <!-- ajouter du php ici recuprer la session du user -->
        <ul id="liste-total">
          <li id="liste-profil">Nom : <?php echo $_SESSION['Nom'] ?></li>
          <li id="liste-profil">Prenom : <?php echo $_SESSION['Prenom'] ?></li>
          <li id="liste-profil">Adresse mail : <?php echo $_SESSION['email'] ?></li>
          <li id="liste-profil">Adresse :<?php echo $_SESSION['Adresse_ligne_1'] ?> </li>
          <li id="liste-profil">Ville :<?php echo $_SESSION['Ville'] ?>  </li>
          <li id="liste-profil">Code Postal :<?php echo $_SESSION['Code_postal'] ?>  </li>
          <li id="liste-profil">Pays : <?php echo $_SESSION['Pays'] ?> </li>
          <li id="liste-profil">Numéro de télephone :<?php echo $_SESSION['Numero_de_telephone'] ?>  </li>
        </ul>
        
      </div>

      <h2 id="title">Informations Bancaire</h2>

      <div class="liste">

      <ul id="liste-total">
        <li id="liste-profil">Type : <?php echo $tpCarte ?></li>
        <li id="liste-profil">Numéro de carte : <?php echo $num ?></li>
        <li id="liste-profil">Validité : <?php echo $date ?></li>
        <li id="liste-profil">Cvv : <?php echo $cvv ?></li>

      </ul>
    </div>

      
    </div> <!-- Fin gauche-->

    <div class="droite">

      <h2 id="title2">Inscrit en tant qu'Acheteur</h2>

      <div id="title2">
         <p>AJOUTER UN MOYEN DE PAIEMENT</p>
      </div>

      <form action="../server/ajoutCarte.php" method="POST">

        <div class="forW">
          
          <div class="wrapper">
            
            <input type="radio" id="visa" name="type_carte" value="visa" checked>
            <label for="visa"  class="option visa"> <div class="dot"></div> <span>VISA</span> </label>
            
            <input type="radio" id="amex" name="type_carte" value="amex" >
            <label for="amex"  class="option amex"> <div class="dot"></div> <span>AMEX</span> </label>
            
            <input type="radio" id="mastercard" name="type_carte" value="mastercard" >
            <label for="mastercard"  class="option mastercard"> <div class="dot"></div> <span>M.CARD</span> </label>
            
          </div>
          
          <div class="numero-carte">
            <input type="text" name="numero"  placeholder="XXXX-XXXX-XXXX-XXXX" minlength="16" maxlength="16" style="height:52px;background: rgba(208, 208, 208, 0.41);" required>
          </div>
          
          <div class="cvv">
            <input type="text" name="cvv" placeholder="CVV" minlength="3" maxlength="3" size="11" style="height:52px;background: rgba(208, 208, 208, 0.41);" required>
          </div>  
          
          <div class="date">
            <input type="date" name="dates" placeholder="EXPIRATION" minlength="5" maxlength="5" size="22" style="height:52px;background: rgba(208, 208, 208, 0.41);" required>
          </div>    
          
          <div class="bloc19">
            <input type="submit" name="submit" name="type_achat"value="CONFIRMER">
          </div>
        </form>
      <!-- Fin forW-->

            <div class="messCarte">
              <p> <?php echo $messErreur ?> </p>
            </div>

      </div>
      
    </div> <!-- Fin droite-->
  
   

  <!-- footer -->

  <footer>
    <div class="contenu">
      <p>
        ECE &copy; 2021 &nbsp;&nbsp;|&nbsp;&nbsp;
        <a href="#"> Mentions légales</a> &nbsp;&nbsp;|&nbsp;&nbsp;

        <a href="mailto:ecemarketplace@gmail.com"> Contactez-nous </a> &nbsp;&nbsp;|&nbsp;&nbsp;
        
        <a href="https://www.google.fr/maps/place/Rue+de+Mouza%C3%AFa,+75019+Paris/@48.8800816,2.395517,17z/data=!3m1!4b1!4m13!1m7!3m6!1s0x47e66dc8c1efd7e7:0x50b82c368941b70!2s19e+Arrondissement+de+Paris,+75019+Paris!3b1!8m2!3d48.8822253!4d2.3819534!3m4!1s0x47e66dbc06038bad:0x1c594ba784c8445b!8m2!3d48.8800816!4d2.3977057">
          Localisation</a>
      </p>
    </div>
  </footer>

  <!-- fin contenu -->
  </div>

    <script src="par.js"></script>

</body>

</html>


<?php }else{
  header('Location: authentification.php');} 
  ?>