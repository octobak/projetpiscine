<?php
  session_start();
  if ($_SESSION["CodeVendeur"]){
?>

<!DOCTYPE html>
<html>

<head>
  <meta charset="utf-8" />
  <html lang="fr">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <link rel="stylesheet" type="text/css" href="../../css/profilV.css">
  <link rel="preconnect" href="https://fonts.gstatic.com">
<link href="https://fonts.googleapis.com/css2?family=Mulish:wght@500&display=swap" rel="stylesheet">
<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>

</head>

<body>
  
  <div id="englobe">

    <!-- navBar -->
    <nav class="navBar">
      <ul>

        <li><a class="active" href="#home">ECE VENDEUR<br>MARKETPLACE</a></li>

        <li>
          <div class="bloc">
            <a href="accueilV.php">
              <p>Accueil</p>
            </a>
          </div>
        </li>

        <li>
          <div class="bloc1">
            <a href="ventes.php">Mes Ventes</a>
          </div>
        </li>


        <li>
          <div class="bloc2">
            <a href="notificationV.php">Notifications</a>
          </div>
        </li>

        <li>
            <div class="idBonjour">
              <a> <span>Bonjour</span> <?php echo $_SESSION['Prenom'] ?></a>
            </div>
          </li>


        <li style="float:right" class="nav-item dropdown">
          <a href="#">
            <button class="cad">
              <img src="../../images/caddie.png" class="rounded-circle" height="40" alt="caddie" loading="lazy" />
            </button>
          </a>
        </li>

        <li style="float:right" class="nav-item dropdown">
          <div class="dropdown">
            <button class="dropbtn">
            <img src="../../images/profil.png" class="rounded-circle" height="40" alt="profil" loading="lazy" />
            </button>
            <div class="dropdown-content">
              <a href="profilV.php">Profil</a>
              <a href="../server/deconnexion.php">Déconnexion</a>
              <a href="#">Test</a>
            </div>
          </div>
        </li>
      </ul>
    </nav>
   <!-- contenu -->
  
    <div class="gauche">

      
      <h2 id="title">Informations Personnelles</h2>
      
      
      <div class="pprofil">
        <img src="../../images/profil.png" class="rounded-circle" height="180" alt="profilP" loading="lazy" />
      </div>
      
      <div class="liste">
        <!-- ajouter du php ici recuprer la session du user -->
        <ul id="liste-total">
          <li id="liste-profil">Nom : <?php echo $_SESSION['Nom'] ?></li>
          <li id="liste-profil">Prenom : <?php echo $_SESSION['Prenom'] ?></li>
          <li id="liste-profil">Adresse mail : <?php echo $_SESSION['email'] ?></li>
          <li id="liste-profil">Adresse :<?php echo $_SESSION['Adresse_ligne_1'] ?> </li>
          <li id="liste-profil">Ville :<?php echo $_SESSION['Ville'] ?>  </li>
          <li id="liste-profil">Code Postal :<?php echo $_SESSION['Code_postal'] ?>  </li>
          <li id="liste-profil">Pays : <?php echo $_SESSION['Pays'] ?> </li>
          <li id="liste-profil">Numéro de télephone :<?php echo $_SESSION['Numero_de_telephone'] ?>  </li>
        </ul>
        
      </div>

    </div> <!-- Fin gauche-->

    <div class="droite">

      <h2 id="title2">Inscrit en tant que Vendeur</h2>

      
      
    </div> <!-- Fin droite-->
  
   

  <!-- footer -->

  <footer>
    <div class="contenu">
      <p>
        ECE &copy; 2021 &nbsp;&nbsp;|&nbsp;&nbsp;
        <a href="#"> Mentions légales</a> &nbsp;&nbsp;|&nbsp;&nbsp;

        <a href="mailto:ecemarketplace@gmail.com"> Contactez-nous </a> &nbsp;&nbsp;|&nbsp;&nbsp;
        
        <a href="https://www.google.fr/maps/place/Rue+de+Mouza%C3%AFa,+75019+Paris/@48.8800816,2.395517,17z/data=!3m1!4b1!4m13!1m7!3m6!1s0x47e66dc8c1efd7e7:0x50b82c368941b70!2s19e+Arrondissement+de+Paris,+75019+Paris!3b1!8m2!3d48.8822253!4d2.3819534!3m4!1s0x47e66dbc06038bad:0x1c594ba784c8445b!8m2!3d48.8800816!4d2.3977057">
          Localisation</a>
      </p>
    </div>
  </footer>

  <!-- fin contenu -->
  </div>

    <script src="par.js"></script>

</body>

</html>


<?php }else{
  header('Location: authentification.php');} 
  ?>