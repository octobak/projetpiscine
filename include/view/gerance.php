<?php
session_start();


$database = "marketplace";
$db_handle = mysqli_connect('localhost', 'root', '');
$db_found = mysqli_select_db($db_handle, $database);

$sql0 = "SELECT CodeVendeur FROM vendeur";
$result0 = mysqli_query($db_handle, $sql0);
$codevendeur = isset($_POST["delVendeur"]) ? $_POST["delVendeur"] : "";

$error = '';
$test = '';


if (isset($_POST['envoie_suppr'])) {
  if (isset($_POST['delVendeur'])) {

    if ($db_found) {

      $sql1 = "DELETE FROM vendeur WHERE CodeVendeur =  $codevendeur";
      $result1 = mysqli_query($db_handle, $sql1);
      $sql2 = "DELETE FROM article WHERE CodeVendeur = $codevendeur";
      $result2 = mysqli_query($db_handle, $sql2);
      $sql3 = "DELETE FROM messages WHERE CodeVendeur = $codevendeur";
      $result3 = mysqli_query($db_handle, $sql3);

      $error = "Le vendeur a été supprimé";
    } else {
      $error = "Sélectionnez un vendeur à supprimer";
      $test = 0;
    }
  }
}

$vendeur = mysqli_query($db_handle, 'SELECT * FROM vendeur ORDER BY CodeVendeur');

if ($_SESSION["IDAdministrateur"]) {
?>


  <!DOCTYPE html>
  <html lang="fr">

  <head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" media="screen" href="../../css/gerance.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
    <link rel="preconnect" href="https://fonts.gstatic.com">
<link href="https://fonts.googleapis.com/css2?family=Mulish:wght@500&display=swap" rel="stylesheet">

    <title>Gerance Admin</title>
  </head>

  <body>

    <div id="englobe">

      <nav class="navBar">
        <ul>

          <li><a class="active" href="#home">ECE Admin<br>MARKETPLACE</a></li>

          <li>
            <div class="bloc">
              <a href="accueilADMIN.php">
                <p>Accueil</p>
              </a>
            </div>
          </li>


          <li>
            <div class="bloc1">
              <a href="gerance.php">Info-Gerance</a>
            </div>
          </li>

          <li>
            <div class="idBonjour">
              <a> <span>Bonjour</span> <?php echo $_SESSION['Prenom'] ?></a>
            </div>
          </li>


          <li style="float:right" class="nav-item dropdown">
            <div class="dropdown">
              <button class="dropbtn">
                <img src="../../images/profil.png" class="rounded-circle" height="40" alt="profil" loading="lazy" />
              </button>
              <div class="dropdown-content">
                <a href="profilADMIN.php">Profil</a>
                <a href="../server/deconnexion.php">Déconnexion</a>
                <!-- <a href="#">Test</a> -->
              </div>
            </div>
          </li>
        </ul>
      </nav>

      <!-- Contenu de la page -->
      <div class="vendeur-suppr">Liste de vendeur sur ECE marcketPlace</div>

      <form method="POST">

        <div class="multi-selectbox">

          <select name="delVendeur" class="selectbox-scrollable" multiple="multiple" size="15">
            <option value="">--------------------Liste de vendeur--------------------</option>
            <?php $i = 1;
            $j = 0; ?>
            <?php while ($data = mysqli_fetch_assoc($vendeur)) { ?>

              <optgroup label="Vendeur <?php echo $i?>">
                <option class="titre-vendeur"> <?php echo "Vendeur numéro : " . $i ?>; </option>
                <option class="info-vendeur"> <?= $data['Prenom'] . " ", $data['Nom'] . " " ?> </option>
                <option class="cv"> <?= $data['CodeVendeur'] ?> </option>
                <option class="espace">-----------------------------------------------------------</option>
              </optgroup>
            <?php
              $i += 1;
            } ?>

          </select>
          <br>

        </div>

        <div class="button">
          <input type="submit" name="envoie_suppr" value="Confirmer" class="supprimer">
        </div>

        <div class="answer">
          <?php echo $error . ""; ?>
        </div>

      </form>

      <!--Fin du contenu  -->

      <!-- footer -->
      <footer>
        <div class="contenu">
          <p>
            ECE &copy; 2021 &nbsp;&nbsp;|&nbsp;&nbsp;
            <a href="#"> Mentions légales </a> &nbsp;&nbsp;|&nbsp;&nbsp;

            <a href="mailto:ecemarketplace@gmail.com"> Contactez-nous </a> &nbsp;&nbsp;|&nbsp;&nbsp;
            <a href="https://www.google.fr/maps/place/Rue+de+Mouza%C3%AFa,+75019+Paris/@48.8800816,2.395517,17z/data=!3m1!4b1!4m13!1m7!3m6!1s0x47e66dc8c1efd7e7:0x50b82c368941b70!2s19e+Arrondissement+de+Paris,+75019+Paris!3b1!8m2!3d48.8822253!4d2.3819534!3m4!1s0x47e66dbc06038bad:0x1c594ba784c8445b!8m2!3d48.8800816!4d2.3977057">
              Localisation</a>
          </p>
        </div>
      </footer>
    </div> <!-- fin englobe-->

    <script src="../../js/scroll.js"></script>
  </body>

  </html>

<?php } else {
  header('Location: authentification.php');
}
?>