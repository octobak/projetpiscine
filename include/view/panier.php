<?php
session_start();
$database = "marketplace";
$db_handle = mysqli_connect('localhost', 'root', '');
$db_found = mysqli_select_db($db_handle, $database);
$CodeClient = $_SESSION["CodeClient"];
//echo $CodeClient;
if ($db_found) {
  $sql = "SELECT *
    FROM Article
    JOIN panier ON Article.IDArticle = panier.Article
    WHERE panier.CodeClient = $CodeClient";
  $result = mysqli_query($db_handle, $sql);
  $panier = array();

  $i = 0;
  while ($data = mysqli_fetch_assoc($result)) {
    $panier[$i]["IDArticle"] = $data["Article"];
    $panier[$i]["Nom"] = $data["Nom"];
    $panier[$i]["Descriptions"] = $data["Descriptions"];
    $panier[$i]["Prix"] = $data["Prix"];
    $panier[$i]["Image1"] = $data["Image1"];
    $panier[$i]["IDPanier"] = $data["ID_panier"];

    $i += 1;
  }
  //echo  $panier[0]["Nom"];
  mysqli_close($db_handle);
  //$tab = array();
  //$tab["paul"] = "waligora";
  //echo $tab["paul"];

}
?>

<!DOCTYPE html>
<html lang="fr">

<head>
  <meta charset="UTF-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <link rel="stylesheet" media="screen" href="../../css/panier.css">
  <link rel="preconnect" href="https://fonts.gstatic.com">
<link href="https://fonts.googleapis.com/css2?family=Mulish:wght@500&display=swap" rel="stylesheet">
  <script src="../../js/panier.js"></script>
  <title>Document</title>
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
</head>

<body>

  <div id="englobe">


    <nav class="navBar">
      <ul>

        <li><a class="active" href="#home">ECE<br>MARKETPLACE</a></li>

        <li>
          <div class="bloc">
            <a href="accueilC.php">
              <p>Accueil</p>
            </a>
          </div>
        </li>


        <li>
          <div class="bloc1">
            <a href="parcourir.php"><span id="mes-ventes">Parcourir</span></a>
          </div>
        </li>


        <li>
          <div class="bloc2">
            <a href="notificationA.php">Notifications</a>
          </div>
        </li>

        <li>
            <div class="idBonjour">
              <a> <span>Bonjour</span> <?php echo $_SESSION['Prenom'] ?></a>
            </div>
          </li>

        <li style="float:right" class="nav-item dropdown">
          <a href="panier.php">
            <button class="cad">
              <img src="../../images/caddie.png" class="rounded-circle" height="40" alt="caddie" loading="lazy" />
            </button>
          </a>
        </li>

        <li style="float:right" class="nav-item dropdown">
          <div class="dropdown">
            <button class="dropbtn">
              <img src="../../images/profil.png" class="rounded-circle" height="40" alt="profil" loading="lazy" />
            </button>
            <div class="dropdown-content">
              <a href="profilA.php">Profil</a>
              <a href="../server/deconnexion.php">Déconnexion</a>
              <!-- <a href="#">Test</a> -->
            </div>
          </div>
        </li>

      </ul>

    </nav>

    <!-- Contenu de la page -->
    <h1>Votre Panier </h1>
    <h3>Sous Total: (Aucun article sélectionné) </h3>
    <div class="button"><button>Passer la commande</button></div>

    <div class="AllPanier">
      <!-- <div class="article">
        <hr>
        <div class="image"><img src="https://cdn.pixabay.com/photo/2020/07/24/21/58/lemon-5435158__340.jpg" ></div>
        <div class="description"> <b>Macbook Pro</b> <br> Lorem, ipsum dolor sit amet consectetur adipisicing elit. Impedit voluptate in id blanditiis eaque at vitae quis libero? Ab impedit omnis fugiat molestiae voluptas quas, quae ducimus delectus natus aperiam!
        Sint optio consectetur natus ex aut maxime provident tenetur. Perferendis fugiat sed tempore dolores, voluptate facilis quo aut qui beatae. Consectetur provident nemo quod eum cum recusandae, tempora laboriosam ipsam.
        Culpa dolore nesciunt facilis aut iusto, velit quisquam blanditiis ipsum architecto omnis quo perferendis magnam dolorum debitis saepe accusantium. Eligendi eos vel aspernatur praesentium minus, sint provident esse pariatur aliquam!</div>
        <div class="prix">Prix: 200€</div>
      </div>
      <div class="article">
        <hr>
        <div class="image"><img src="https://cdn.pixabay.com/photo/2020/07/24/21/58/lemon-5435158__340.jpg" ></div>
        <div class="description"> Macbook Pro <br> Lorem, ipsum dolor sit amet consecte</div>
        <div class="prix">Prix: 200€</div>
      </div> -->
    </div>
    <div class="hr">
      <hr>
    </div>
    <div id="notification" style="display: none;">
      <span class="dismiss"><a title="dismiss this notification">x</a></span>
    </div>
    <script>
      var panier = <?php echo json_encode($panier); ?>;
      //console.log(panier)
      var total = 0;
      var selection = 0;
      var IDArticles = [];

      function load() {
        $('.AllPanier').empty();
        for (var i = 0; i < panier.length; i++) {
          
          console.log(panier[0])
          $('.AllPanier').append(
            `<div class="article">
            <hr>
            <div class="checkbox" ><input type="checkbox" indiceArticle=${i}></div>
            <div class="image"><img src=${panier[i]['Image1']} ></div>
            <div class="description"><span class="titre">${panier[i]['Nom']}</span><br><b>Description:  </b>${panier[i]['Descriptions']}</description></div>
            <div class="prix">Prix: ${panier[i]['Prix']} €</div>
            <div class="supprimer"><button class="suppr" indiceArticle=${i} >Supprimer</button></div>
            </div>`
          )
          console.log("load")
        }
      }
      load();
      
      $(document).ready(function() {
        $('.suppr').click(()=>{
          console.log("test81")
        })
        $('.suppr').click(
          function() { 
          indiceArticle = this.getAttribute('indiceArticle')
          console.log("test")
          IDPanier = panier[indiceArticle]["IDPanier"]
          $.post("../server/suppr.php", {
            IDPanier:IDPanier
          }, function(data, status){
            $("#test").html(data);
            $.get("../server/refresh.php", function(data) {
                $("#test").html(data);
                test = JSON.parse(data);
                panier = test;
                console.log("ici")
                console.log(panier.length)
                
                load();
              });
          })
        })
        $('.button').click(
          function() {
            
            IDArticles1 = JSON.stringify(IDArticles)
            $.post("../server/commander.php", {
              IDArticles: IDArticles1
            }, function(data, status) {
              $("#test").html(data);
              $("#notification").fadeIn("slow").append('Votre commande a bien été prise en compte !');
              $(".dismiss").click(function() {
                $("#notification").fadeOut("slow");
              });
              $.get("../server/refresh.php", function(data) {
                $("#test").html(data);
                test = JSON.parse(data);
                panier = test;
                load();
              });
              //load();

            })
          })


        $('input').change(
          function() {
            if ($(this).is(':checked')) {
              indiceArticle = this.getAttribute('indiceArticle')
              prix = panier[indiceArticle]["Prix"];
              IDArticles.push(parseInt(panier[indiceArticle]["IDArticle"]));
              total += parseInt(prix);
              selection += 1;

              //console.log(IDArticles)
              if (selection > 1) $('h3').html(`Sous total: (${selection} articles) ${total} €`)
              else $('h3').html(`Sous total: (${selection} article) ${total} €`)
              //console.log(this)
            }
            if (!($(this).is(':checked'))) {
              indiceArticle = this.getAttribute('indiceArticle')
              //console.log(IDArticles)
              const index = IDArticles.indexOf(parseInt(panier[indiceArticle]["IDArticle"]));
              IDArticles.splice(index, 1);
              //console.log(IDArticles)
              prix = panier[indiceArticle]["Prix"];
              total -= parseInt(prix);

              selection -= 1;
              if (selection == 0) $('h3').html(`Sous Total: (Aucun article sélectionné)`)
              if (selection == 1) $('h3').html(`Sous total: (${selection} article) ${total} €`)
              if (selection > 1) $('h3').html(`Sous total: (${selection} articles) ${total} €`)

            }
          });



      });
    </script>
    

    <!--Fin du contenu  -->

    <!-- footer -->
    <footer>
      <div class="contenu">
        <p>
          ECE &copy; 2021 &nbsp;&nbsp;|&nbsp;&nbsp;
          <a href="#"> Mentions légales</a> &nbsp;&nbsp;|&nbsp;&nbsp;

          <a href="mailto:ecemarketplace@gmail.com"> Contactez-nous </a> &nbsp;&nbsp;|&nbsp;&nbsp;
          
          <a href="https://www.google.fr/maps/place/Rue+de+Mouza%C3%AFa,+75019+Paris/@48.8800816,2.395517,17z/data=!3m1!4b1!4m13!1m7!3m6!1s0x47e66dc8c1efd7e7:0x50b82c368941b70!2s19e+Arrondissement+de+Paris,+75019+Paris!3b1!8m2!3d48.8822253!4d2.3819534!3m4!1s0x47e66dbc06038bad:0x1c594ba784c8445b!8m2!3d48.8800816!4d2.3977057">
            Localisation</a>
        </p>
      </div>
    </footer>
  </div> <!-- fin englobe-->
  <div id="test"></div>

</body>

</html>