<?php
session_start();
$database = "marketplace";
$db_handle = mysqli_connect('localhost', 'root', '');
$db_found = mysqli_select_db($db_handle, $database);

if ($db_found) {
  $sql = "SELECT * from article ";
  $result = mysqli_query($db_handle, $sql);
  $articles = array();

  $i = 0;
  while ($data = mysqli_fetch_assoc($result)) {
    $nom = $data['Nom'];
    $prix = $data["Prix"];
    $image1 = $data["Image1"];
    $type = $data["type_achat"];
    $description = $data["Descriptions"];
    $IDArticle = $data["IDArticle"];

    $articles[$i]["nom"] = $nom;
    $articles[$i]["prix"] = $prix;
    $articles[$i]["image1"] = $image1;
    $articles[$i]["type"] = $type;
    $articles[$i]["description"] = $description;
    $articles[$i]["IDArticle"] = $IDArticle;

    $i += 1;
  }

  mysqli_close($db_handle);
  //$tab = array();
  //$tab["paul"] = "waligora";
  //echo $tab["paul"];
}
if ($_SESSION["CodeClient"]) {
?>

  <!DOCTYPE html>
  <html lang="fr">

  <head>
    <meta charset="UTF-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <title>Document</title>
    <link rel="stylesheet" href="../../css/parcourir.css" type="text/css" />
    <link rel="preconnect" href="https://fonts.gstatic.com">
<link href="https://fonts.googleapis.com/css2?family=Mulish:wght@500&display=swap" rel="stylesheet">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
    <script>
      var articles = <?php echo json_encode($articles); ?>;
    </script>
    <script>
      console.log(articles)
      $(document).ready(function() {
        $('#ajouterPanier').click(function() {
          var IDAjouter = this.parentElement.getAttribute('numeroArticle');
          var IDArticle = articles[IDAjouter]["IDArticle"];
          console.log(IDArticle)
          $.post("../server/ajoutPanier.php", {
            IDArticle: IDArticle
          }, function(data, status) {
            $("#test").html(data);
            $("#modal_container").removeClass("show");
            alert("Vous avez bien ajouté l'article au panier")
            //modal_container.classList.remove("show");
          })
        })
      });
      var dialog = $(foo).dialog('open');
      setTimeout(function() {
        dialog.dialog('close');
      }, time);
    </script>
  </head>

  <body>
    <nav class="navBar">
      <ul>
        <li>
          <a class="active" href="#home">ECE<br />MARKETPLACE</a>
        </li>

        <li>
          <div class="bloc1">
            <a href="accueilC.php"> Accueil </a>
          </div>
        </li>

        <li>
          <div class="bloc1">
            <a href="parcourir.php">Parcourir</a>
          </div>
        </li>

        <li>
          <div class="bloc2">
            <a href="notificationA.php">Notifications</a>
          </div>
        </li>

        <li>
            <div class="idBonjour">
              <a> <span>Bonjour</span> <?php echo $_SESSION['Prenom'] ?></a>
            </div>
          </li>

        <li style="float: right" class="nav-item dropdown">
          <a href="panier.php">
            <button class="cad">
              <img src="../../images/caddie.png" class="rounded-circle" height="40" alt="caddie" loading="lazy" />
            </button>
          </a>
        </li>

        <li style="float: right" class="nav-item dropdown">
          <div class="dropdown">
            <button class="dropbtn">
              <img src="../../images/profil.png" class="rounded-circle" height="40" alt="profil" loading="lazy" />
            </button>
            <div class="dropdown-content">
              <a href="profilA.php">Profil</a>
              <a href="../server/deconnexion.php">Déconnexion</a>
            </div>
          </div>
        </li>
      </ul>
    </nav>
    <h2 id="titre-article">Articles mis en ligne</h2>
    <div class="grille">
      <!-- <div class="grid-item" onclick="window.location='accueilC.php'">
        <div class="photo"><img style="width: 400px; height:190px;" src="https://cdn.pixabay.com/photo/2020/07/24/21/58/lemon-5435158__340.jpg"></div>
        <div class="description">
          <div class="Prix">10 €</div>
          <div class="Nom">Pomme</div>
          <div class="Type">Achat direct</div>
        </div>      
      </div> -->
    </div>
    <div class="modal-container" id="modal_container">
      <div class="modal">
        <h1></h1>
        <h2>1300 €</h2>
        <h3>En Achat direct!</h3>
        <img src="https://cdn.pixabay.com/photo/2020/07/24/21/58/lemon-5435158__340.jpg" style="width: 400px; height:190px; border-radius: 10px; margin: 30px">
        <h4>Description: </h4>
        <p>Lorem ipsum dolor, sit amet consectetur adipisicing elit. Quisquam suscipit distinctio a inventore quibusdam laudantium ipsam sint atque tenetur veniam dolores praesentium voluptatum cupiditate, libero voluptatem quod repellendus, quia facilis!
          Non laborum tempore doloribus harum, error commodi voluptatum, expedita laudantium, dolorum dolore laboriosam ratione alias earum deleniti asperiores soluta et qui. Ex cupiditate labore nisi, quae ducimus dolorum deleniti eveniet?</p>
        <button id="ajouterPanier">Ajouter au panier</button>
        <button id="close">Fermer</button>
      </div>
    </div>
    <div id="test"></div>
    <script>
      $(document).on("click", ".bEncherir", function() {
        valeur = $('.encherir').val()
        console.log(valeur)
        indiceArticle = this.parentElement.getAttribute('numeroarticle')
        IDArticle = articles[indiceArticle]['IDArticle']
        console.log(IDArticle)

        $.post("../server/encherir.php", {
          encherir: valeur,
          IDArticle: IDArticle
        }, function(data, status) {
          $("#test").html(data);
          alert("Vous avez bien augmenté le prix !")
          location.reload();
        });

      });
      $(document).ready(function() {
        $('.grid-item').click(
          function() {
            console.log("cc")
            numArticle = this.getAttribute('numeroarticle');
            if (articles[numArticle]["type"] == "enchere") {
              console.log("test")
              $('.modal-container .modal').attr('numeroarticle', numArticle)
              $('.modal-container').addClass('show');
              $('.modal-container h1').html(articles[numArticle]["nom"])
              $('.modal-container h2').html(articles[numArticle]["prix"] + " €")
              $('.modal-container h3').html(articles[numArticle]["type"])
              $('.modal-container img').attr("src", articles[numArticle]["image1"])
              $('.modal-container p').html(articles[numArticle]["description"])
              $('.modal-container .modal').append("<input type='number' class='encherir' placeholder='Enchérir'></input>")
              $('.modal-container .modal').append("<button class='bEncherir'>Encherir</button>")
              console.log($('#ajouterPanier').is("#ajouterPanier"))
              //$('#ajouterPanier').remove();
              console.log($('#ajouterPanier').is("#ajouterPanier"))

            }
          }
        )

        $('#close').click(
          function() {
            $('.encherir').remove();
            $('.bEncherir').remove();
            if (!($('#ajouterPanier').is("#ajouterPanier"))) {
              $('.modal-container .modal').append("<button id='ajouterPanier'>Ajouter au panier</button>")
            }
          }
        )
      });
    </script>
  </body>


  <!-- Script JS pour ajouter dynamiquement les images dans la catégorie PARCOURIR -->
  <?php $message = "Hello Folks"; ?>

  <script src="../../js/parcourir.js"></script>

  </html>
<?php } else {
  header('Location: authentification.php');
} ?>