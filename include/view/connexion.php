<?php session_start();?>
<!DOCTYPE html>
<html lang="fr">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
    <link rel="stylesheet" href="../../css/connexion.css" media="screen" type="text/css" />
    <link rel="preconnect" href="https://fonts.gstatic.com">
<link href="https://fonts.googleapis.com/css2?family=Mulish:wght@500&display=swap" rel="stylesheet">
</head>
<body>
    <?php 
    if(isset($_SESSION['email'])){
        echo "vous êtes connecté en tant que : " . $_SESSION['email'];
    } else {
        ?>
   <div id="container">
            <!-- zone de connexion -->
            
            <form action="../server/verification.php" method="POST">
                <h1>CONNEXION</h1>
                
                <label><b>Adresse Mail</b></label>
                <input type="text" placeholder="Entrer l'adresse mail" name="email" required>

                <label><b>Mot de passe</b></label>
                <input type="password" placeholder="Entrer le mot de passe" name="password" required>

                <input type="submit" id='submit' value='LOGIN' name="login" >
               
            </form>
        </div>
    <?php 
    }
    ?>
</body>
</html>