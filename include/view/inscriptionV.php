<!DOCTYPE html>
<html>
<head>
    <title>Page de connexion</title>
    <meta charset="utf-8">
        <!-- importer le fichier de style -->
        <link rel="stylesheet" href="../../css/inscription.css" media="screen" type="text/css" />
        <link rel="preconnect" href="https://fonts.gstatic.com">
<link href="https://fonts.googleapis.com/css2?family=Mulish:wght@500&display=swap" rel="stylesheet">
    </head>
    <body>
        <div id="container">
            <!-- zone de connexion -->
            
            <form action="../server/inscriptionV.php" method="POST">
                <h1>INSCRIPTION <br>Vendeur</h1>
                
                <div id="nav">
        <label><b>Nom</b></label><br>
                <input type="text" placeholder="Entrer votre nom" name="nom" required autofocus  size="30"><br>

        <label><b>  Prénom</b></label><br>
                <input type="text" placeholder="Entrer votre prénom" name="prenom" required autofocus  size="30"><br>

        <label><b>Adresse Mail</b></label><br>
                <input type="text" placeholder="Entrer l'adresse mail" name="email" required autofocus  size="30"><br>

                <label><b>Mot de passe</b></label><br>
                <input type="password" placeholder="Entrer le mot de passe" name="password" required autofocus  size="30"><br>
                <label><b>Confirmer votre Mot de passe</b></label><br>
                <input type="password" placeholder="Entrer le même mot de passe" name="password2" required autofocus  size="30">
    </div>

    <div id="section">
         <label><b>Adresse ligne 1</b></label><br>
                <input type="text" placeholder="Entrer votre adresse" name="adresse" required autofocus  size="30"><br>

        <label><b>  Pays</b></label><br>
                <input type="text" placeholder="Entrer votre pays" name="pays" required autofocus  size="30"><br>

        <label><b>Code postal</b></label><br>
                <input type="text" placeholder="Entrer votre code postal" name="code_postal" required autofocus  size="30"><br>

                <label><b>Ville</b></label><br>
                <input type="text" placeholder="Entrer votre ville" name="ville" required autofocus  size="30"><br>
                <label><b>Numéro de téléphone</b></label><br>
                <input type="text" placeholder="Entrer votre numéro de téléphone" name="numero_telephone" required autofocus size="30">
    </div>

             <input type="submit" id='submit' value='Créer le compte' name ='button' >
                 
               
            </form>
        </div>
    </body>

</html>