<?php
session_start();

$bdd = new PDO('mysql:host=localhost; dbname=marketplace;charset=utf8', 'root', '');

if (isset($_POST['envoie_message'])) {
  if (isset($_POST['destinataire'], $_POST['message']) && !empty($_POST['destinataire']) && !empty($_POST['message'])) {
    $destinataire = htmlspecialchars($_POST['destinataire']);
    $message = htmlspecialchars($_POST['message']);
    $PV = 0;
    $PA = 1;

    $id_destinataire = $bdd->prepare('SELECT CodeVendeur FROM vendeur WHERE Nom = ?');
    $id_destinataire->execute(array($destinataire));
    $id_destinataire = $id_destinataire->fetch();
    $id_destinataire = $id_destinataire['CodeVendeur'];

    //expediteur = client et destinataire = vendeur
    //code client et code vendeur sont inversé ici pour garde les variables de sessions
    $ins = $bdd->prepare('INSERT INTO messages (CodeClient,CodeVendeur,Mess,pv,pa) VALUES (?,?,?,?,?)');
    $ins->execute(array($_SESSION['CodeClient'], $id_destinataire, $message, $PV, $PA));

    $error = "Votre message a bien été envoyé";
  } else {
    $error = "compléter tous les champs";
  }
}

$destinataires = $bdd->query('SELECT Nom FROM vendeur');

//<!-- PHP pour la partie envoie de message -->

//<!--  Debut Reception de message -->

//$bdd = new PDO('mysql:host=localhost; dbname=marketplace;charset=utf8', 'root', '');

$msg = $bdd->prepare('SELECT * FROM messages  WHERE CodeClient =? AND pv = 1 AND pa = 0 ');
$msg->execute(array($_SESSION['CodeClient']));
$msg_nbr = $msg->rowCount();

?>
<!-- reception de message -->

<!DOCTYPE html>
<html lang="fr">

<head>
  <meta charset="UTF-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <link rel="stylesheet" media="screen" href="../../css/notificationA.css">
  <link rel="preconnect" href="https://fonts.gstatic.com">
<link href="https://fonts.googleapis.com/css2?family=Mulish:wght@500&display=swap" rel="stylesheet">
<link rel="preconnect" href="https://fonts.gstatic.com">
<link href="https://fonts.googleapis.com/css2?family=Mulish:wght@500&display=swap" rel="stylesheet">

  <title>Document</title>
</head>

<body>

  <div id="englobe">


    <nav class="navBar">
      <ul>

        <li><a class="active" href="#home">ECE<br>MARKETPLACE</a></li>

        <li>
          <div class="bloc">
            <a href="accueilC.php">
              <p>Accueil</p>
            </a>
          </div>
        </li>


        <li>
          <div class="bloc1">
            <a href="parcourir.php">Parcourir</a>
          </div>
        </li>


        <li>
          <div class="bloc2">
            <a href="notificationA.php">Notifications</a>
          </div>
        </li>

        <li>
            <div class="idBonjour">
              <a> <span>Bonjour</span> <?php echo $_SESSION['Prenom'] ?></a>
            </div>
          </li>

        <li style="float:right" class="nav-item dropdown">
          <a href="panier.php">
            <button class="cad">
              <img src="../../images/caddie.png" class="rounded-circle" height="40" alt="caddie" loading="lazy" />
            </button>
          </a>
        </li>

        <li style="float:right" class="nav-item dropdown">
          <div class="dropdown">
            <button class="dropbtn">
              <img src="../../images/profil.png" class="rounded-circle" height="40" alt="profil" loading="lazy" />
            </button>
            <div class="dropdown-content">
              <a href="profilA.php">Profil</a>
              <a href="../server/deconnexion.php">Déconnexion</a>
              <!-- <a href="#">Test</a> -->
            </div>
          </div>
        </li>

      </ul>

    </nav>

    <!-- Contenu de la page -->

    <div class="all">

      <!-- Debut Envoie de message -->
      <form method="POST">

        <label class="titre-bar">Destinataire : </label>
        <select name="destinataire" class="select-bar">

          <?php while ($data = $destinataires->fetch()) { ?>

            <option> <?php echo $data['Nom'] ?> </option>

          <?php } ?>
        </select>

        <textarea class="zone-txt" placeholder="Votre message" name="message" cols="60" rows="5"></textarea>
        <br>
        <br>
        <div class="error-msg">
          <?php if (isset($error)) {
            echo $error;
          } ?>
        </div>

        <input class="envoie-msg" type="submit" name="envoie_message" value="Envoyer">
        <br><br>
      </form>
      <!-- Fin envoie de message -->

      <div class="all-messages">

        <!-- Début Affichage des msg -->
        <?php
        if ($msg_nbr == 0) {
          echo "vous n'avez pas de message";
        }

        while ($m = $msg->fetch()) {
          $PV = 1;
          $PA = 0;
          $exp_acheteur =  $bdd->prepare('SELECT Nom,pv,pa FROM vendeur as v, messages as m WHERE v.CodeVendeur =? AND m.pv = 1 AND m.pa = 0');
          $exp_acheteur->execute(array($m['CodeVendeur']));
          $exp_acheteur = $exp_acheteur->fetch();
          $exp_acheteur = $exp_acheteur['Nom'];
        ?>
          <div class="bloc-msg">

            <b class="nom-exp"> <?= $exp_acheteur ?> </b> <span id="poli">vous a envoyé: </span> <br>
            <span id="message"> <?= $m['Mess'] ?> </span>
          </div>

        <?php } ?>
      </div>

    </div>
    <!-- Fin Affichage des msg -->


    <!--Fin du contenu  -->

    <!-- footer -->
    <footer>
      <div class="contenu">
        <p>
          ECE &copy; 2021 &nbsp;&nbsp;|&nbsp;&nbsp;
          <a href="#"> Mentions légales</a> &nbsp;&nbsp;|&nbsp;&nbsp;

          <a href="mailto:ecemarketplace@gmail.com"> Contactez-nous </a> &nbsp;&nbsp;|&nbsp;&nbsp;
         
          <a href="https://www.google.fr/maps/place/Rue+de+Mouza%C3%AFa,+75019+Paris/@48.8800816,2.395517,17z/data=!3m1!4b1!4m13!1m7!3m6!1s0x47e66dc8c1efd7e7:0x50b82c368941b70!2s19e+Arrondissement+de+Paris,+75019+Paris!3b1!8m2!3d48.8822253!4d2.3819534!3m4!1s0x47e66dbc06038bad:0x1c594ba784c8445b!8m2!3d48.8800816!4d2.3977057">
            Localisation</a>
        </p>
      </div>
    </footer>
  </div> <!-- fin englobe-->


</body>

</html>