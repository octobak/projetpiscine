<?php
  session_start();
  if ($_SESSION["CodeVendeur"]){
?>
<!DOCTYPE html>
<html>

<head>
  <meta charset="utf-8" />
  <html lang="fr">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <link rel="stylesheet" type="text/css" href="../../css/accueil.css">
  <link rel="preconnect" href="https://fonts.gstatic.com">
<link href="https://fonts.googleapis.com/css2?family=Mulish:wght@500&display=swap" rel="stylesheet">
</head>

<body>


  <div id="englobe">

    <nav class="navBar">
      <ul>

        <li><a class="active" href="#home">ECE VENDEUR<br>MARKETPLACE</a></li>

        <li>
          <div class="bloc">
            <a href="#news">
              <p>Accueil</p>
            </a>
          </div>
        </li>


        <li>
          <div class="bloc1">
            <a href="ventes.php">Mes ventes</a>
          </div>
        </li>


        <li>
          <div class="bloc2">
            <a href="notificationV.php">Notifications</a>
          </div>
        </li>

        <li>
            <div class="idBonjour">
              <a> <span>Bonjour</span> <?php echo $_SESSION['Prenom'] ?></a>
            </div>
          </li>


        <li style="float:right" class="nav-item dropdown">
          <a href="#">
            <button class="cad">
              <img src="../../images/caddie.png" class="rounded-circle" height="40" alt="caddie" loading="lazy" />
            </button>
          </a>
        </li>

        <li style="float:right" class="nav-item dropdown">
          <div class="dropdown">
            <button class="dropbtn">
            <img src="../../images/profil.png" class="rounded-circle" height="40" alt="profil" loading="lazy" />
            </button>
            <div class="dropdown-content">
              <a href="profilV.php">Profil</a>
              <a href="../server/deconnexion.php">Déconnexion</a>
            </div>
          </div>
        </li>

      </ul>

    </nav>

    <div id="header">
      <h1>VENTE <br> FLASH</h1>
    </div>

    <div id="header1">
      <h4>VOUS AVEZ ENCORE
      <span id="time">6h.30min</span> POUR EN
        PROFITER
      </h4>



      <div class="container">
        <div class="slides">
          <div class="slide">
            <img src="../../images/back1.jpg" alt="superMan" />
          </div>
          <div class="slide">
            <img src="../../images/back2.jpg" alt="batMan" />
          </div>
          <div class="slide">
            <img src="../../images/back3.jpg" alt="ironMan" />
          </div>
          <div class="slide">
            <img src="../../images/back4.jpg" alt="Union" />
          </div>
          <div class="slide">
            <img src="../../images/back5.jpg" alt="Union" />
          </div>
          <div class="slide">
            <img src="../../images/back6.jpg" alt="Union" />
          </div>
          <div class="slide">
            <img src="../../images/back7.jpg" alt="Union" />
          </div>
        </div>
      </div>



      <!-- <div class="slide-controls">
        <button id="prev-btn"> <img src="../images/fleche-droite.png" alt="fleche_gauche" /></button>
        
        <button id="next-btn"><img src="../images/fleche-droite.png" alt="fleche_droite" /></button>
      </div> -->


      <div class="description">
        <p> <br> <br>
        ECE marketPlace est une place de marché dynamique avec une présence internationale. Elle est sans équivalent.Vous souhaitez que les objets que vous vendez soient vus par des millions de personnes dans le monde entier ? Vous êtes au bon endroit. </p>
        En outre, toutes vos transactions sont protégées. Vous pouvez donc vendre vos objets en toute sécurité sur eBay.
<br>
          Vendre sur ECE marketplace, c'est vraiment simple.
      </div>


    </div>

    <footer>
      <div class="contenu">
        <p>
          ECE &copy; 2021 &nbsp;&nbsp;|&nbsp;&nbsp;
          <a href="#"> Mentions légales</a> &nbsp;&nbsp;|&nbsp;&nbsp;

          <a href="mailto:ecemarketplace@gmail.com"> Contactez-nous </a> &nbsp;&nbsp;|&nbsp;&nbsp;
          
          <a href="https://www.google.fr/maps/place/Rue+de+Mouza%C3%AFa,+75019+Paris/@48.8800816,2.395517,17z/data=!3m1!4b1!4m13!1m7!3m6!1s0x47e66dc8c1efd7e7:0x50b82c368941b70!2s19e+Arrondissement+de+Paris,+75019+Paris!3b1!8m2!3d48.8822253!4d2.3819534!3m4!1s0x47e66dbc06038bad:0x1c594ba784c8445b!8m2!3d48.8800816!4d2.3977057">
            Localisation</a>
        </p>
      </div>
    </footer>

    <script src="../../js/slideEffect.js"></script>

</body>

</html>
<?php } else{
  header('Location: authentification.php');
} ?>