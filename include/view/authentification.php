<!DOCTYPE html>
<html lang="fr" class="no-js">
	<head>
		<meta charset="UTF-8">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<title>ECE - MarketPlace</title>
		<meta name="description" content="">
		<meta name="keywords" content="">
		<meta name="author" content="Codrops">
		<link rel="shortcut icon" href="favicon.26242483.ico">
		<link rel="stylesheet" href="https://use.typekit.net/eqx3jwb.css">
		<link rel="stylesheet" type="text/css" href="../../css/style.css">
		<link rel="preconnect" href="https://fonts.gstatic.com">
<link href="https://fonts.googleapis.com/css2?family=Mulish:wght@500&display=swap" rel="stylesheet">
		<script>document.documentElement.className = "js";

var supportsCssVars = function supportsCssVars() {
  var e,
      t = document.createElement("style");
  return t.innerHTML = "root: { --tmp-var: bold; }", document.head.appendChild(t), e = !!(window.CSS && window.CSS.supports && window.CSS.supports("font-weight", "var(--tmp-var)")), t.parentNode.removeChild(t), e;
};

supportsCssVars() || alert("Please view this demo in a modern browser that supports CSS Variables.");</script>
	</head>
	<body class="demo-1 loading">
		<main>
			</div>
			<div class="content">
				<div class="grid">
					<div class="grid__item pos-1"><div class="grid__item-img" style="background-image: url(../../images/back1.jpg)"></div></div>
					<div class="grid__item pos-2"><div class="grid__item-img" style="background-image:url(../../images/back2.jpg);"></div></div>
					<div class="grid__item pos-3"><div class="grid__item-img" style="background-image:url(../../images/back3.jpg);"></div></div>
					<div class="grid__item pos-4"><div class="grid__item-img" style="background-image:url(../../images/back4.jpg);"></div></div>
					<div class="grid__item pos-5"><div class="grid__item-img" style="background-image:url(../../images/back5.jpg);"></div></div>
					<div class="grid__item pos-6"><div class="grid__item-img" style="background-image:url(../../images/back6.jpg);"></div></div>
					<div class="grid__item pos-7"><div class="grid__item-img" style="background-image:url(../../images/back7.jpg);"></div></div>
					<div class="grid__item pos-8"><div class="grid__item-img" style="background-image:url(../../images/back8.jpg);"></div></div>
					<div class="grid__item pos-9"><div class="grid__item-img" style="background-image:url(../../images/back9.jpg);"></div></div>
					<div class="grid__item pos-10"><div class="grid__item-img" style="background-image:url(../../images/back10.jpg);"></div></div>
				</div>
				<h2 class="content__title no-select">
					<span>ECE</span>
					<span class="content__title-sub">MarketPlace</span>
				</h2>

        <h2 class="menu">
					<div class="menu__link con"><a href="../../include/view/connexion.php">Connexion</a></div>
					<div class="menu__link ins1"><a href="../../include/view/inscriptionA.php">Créer un compte client</a></div> 
					<div class="menu__link ins2"><a href="../../include/view/inscriptionV.php">Créer un compte vendeur</a></div>
				</h2>

			</div>
		</main>
      
		<script src="../../js/app.js"></script>
	</body>
</html>