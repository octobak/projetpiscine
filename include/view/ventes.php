<?php
session_start();
if ($_SESSION["CodeVendeur"]) {
?>

  <!DOCTYPE html>
  <html lang="fr">

  <head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" media="screen" href="../../css/ventes.css">
    <link rel="preconnect" href="https://fonts.gstatic.com">
<link href="https://fonts.googleapis.com/css2?family=Mulish:wght@500&display=swap" rel="stylesheet">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
    <script>
      $(document).ready(function() {
          $(".suppr").click(function(){
        var indiceArticle = this.getAttribute('indiceArticle');
        var IDArticle = articles[indiceArticle]["IDArticle"];
        console.log(IDArticle);

        $.post("../server/delete.php",{
          IDArticle : IDArticle
        },function(data,status){
          $('#test').html(data)
        })
      });
    });
    </script>

    <title>Parcourir</title>
  </head>

  <body>

    <div id="englobe">

      <nav class="navBar">
        <ul>

          <li><a class="active" href="#home">ECE VENDEUR<br>MARKETPLACE</a></li>

          <li>
            <div class="bloc">
              <a href="accueilV.php">
                <p>Accueil</p>
              </a>
            </div>
          </li>


          <li>
            <div class="bloc1">
              <a href="ventes.php"><span id="mes-ventes">Mes ventes</span></a>
            </div>
          </li>


          <li>
            <div class="bloc2">
              <a href="notificationV.php">Notifications</a>
            </div>
          </li>

          <li>
            <div class="idBonjour">
              <a> <span>Bonjour</span> <?php echo $_SESSION['Prenom'] ?></a>
            </div>
          </li>


          <li style="float:right" class="nav-item dropdown">
            <a href="#">
              <button class="cad">
                <img src="../../images/caddie.png" class="rounded-circle" height="40" alt="caddie" loading="lazy" />
              </button>
            </a>
          </li>

          <li style="float:right" class="nav-item dropdown">
            <div class="dropdown">
              <button class="dropbtn">
                <img src="../../images/profil.png" class="rounded-circle" height="40" alt="profil" loading="lazy" />
              </button>
              <div class="dropdown-content">
                <a href="profilV.php">Profil</a>
                <a href="../server/deconnexion.php">Déconnexion</a>
                <!-- <a href="#">Test</a> -->
              </div>
            </div>
          </li>

        </ul>

      </nav>

      <div class="ajout-produit">

        <div class="block">


          <h1 id="pd">Ajout de produit</h1>

          <form action="../server/product.php" method="POST">

            <br>
            <p id="title">Type d'achat</p> <br>

            <div class="wrapper">

              <input type="radio" id="vente" name="type_achat" value="vente" checked>
              <label for="vente" class="option vente">
                <div class="dot"></div> <span>Vente</span>
              </label>

              <input type="radio" id="nego" name="type_achat" value="negociation">
              <label for="nego" class="option nego">
                <div class="dot"></div> <span>Négociation</span>
              </label>

              <input type="radio" id="enchere" name="type_achat" value="enchere">
              <label for="enchere" class="option enchere">
                <div class="dot"></div> <span>Enchère</span>
              </label>

            </div>

            <br>
            <p id="title">Nom produit</p>
            <input type="text" name="nom" required>

            <br>
            <p id="title">Description</p>
            <textarea name="descriptions" cols="53" rows="8" required></textarea>
            <br>

            <label for="categorie">
              <p id="title">Categorie:</p>
            </label>

            <select name="categorie" id="categorie">
              <option value="">--OPTION--</option>
              <option value="art">Art</option>
              <option value="technologie">Technologie</option>
              <option value="animaux">Animaux</option>
              <option value="scolaire">Scolaire</option>
              <option value="vetement">Vétement</option>
              <option value="autre">Autre</option>
            </select>
            <br>

            <p id="title">Prix</p>
            <input type="number" name="prix" required>

            <br>
            <p id="title">Photo 1 Produit </p>
            <input type="text" name="image1" placeholder="https://example.com">
            <br>
            <p id="title">Photo 2 Produit</p>
            <input type="text" name="image2" placeholder="https://example.com">
            <br>
            <p id="title">Photo 3 Produit</p>
            <input type="text" name="image3" placeholder="https://example.com">

            <br>
            <input type="submit" name="submit" value="publier">
        </div>

      </div>

      <?php
      $cv = $_SESSION["CodeVendeur"];
      $database = "marketPlace";
      $db_handle = mysqli_connect('localhost', 'root', '');
      $db_found = mysqli_select_db($db_handle, $database);
      if ($db_found) {
        $sql = "SELECT * FROM article WHERE CodeVendeur LIKE $cv";
        $result = mysqli_query($db_handle, $sql);
        $i = 0;
        while ($data = mysqli_fetch_assoc($result)) {

          $nom = $data['Nom'];
          $prix = $data['Prix'];
          $descriptions = $data['Descriptions'];
          $categorie = $data['Categorie'];
          $type_achat = $data['type_achat'];
          $image1 = $data["Image1"];
          $IDArticle = $data["IDArticle"];


          $articles[$i]["nom"] = $nom;
          $articles[$i]["prix"] = $prix;
          $articles[$i]["descriptions"] = $descriptions;
          $articles[$i]["categorie"] = $categorie;
          $articles[$i]["type_achat"] = $type_achat;
          $articles[$i]["image1"] = $image1;
          $articles[$i]["IDArticle"] = $IDArticle;

          $i += 1;
        }
      }
      ?>

      </form>


      <!-- Debut Liste article  -->

      <h3>Articles mis en ligne</h3>
      <div class="del"></div>
      <div class="grille">
        <!-- <div class="grid-item">
        <div class="photo"><img style="width: 400px; height:190px;" src="https://cdn.pixabay.com/photo/2020/07/24/21/58/lemon-5435158__340.jpg"></div>
        <div class="description"></div>      
      </div> -->
      </div>
      <div id="test"></div>
  </body>


  <!-- Script JS pour ajouter dynamiquement les images dans la catégorie PARCOURIR -->

  <script>
    var articles = <?php echo json_encode($articles); ?>;
    for (var i = 0; i < articles.length; i++) {
      var element = document.createElement("div");
      element.classList.add("grid-item")
      //element.appendChild(document.createTextNode('The man who mistook his wife for a hat'));
      document.getElementsByClassName('grille')[0].appendChild(element);
    }

    for (var i = 0; i < articles.length; i++) {

      var element2 = document.createElement("div");
      element2.classList.add("photo")
      document.getElementsByClassName('grid-item')[i].appendChild(element2);

      var element = document.createElement("div");
      element.classList.add("description")
      element.appendChild(document.createTextNode("Produit: " + articles[i]["nom"] + " | Prix: " + articles[i]["prix"] + " €" + " | Description : " + articles[i]["descriptions"]));
      element.appendChild(document.createTextNode(" | Catégorie: " + articles[i]["categorie"] + " | Type achat: " + articles[i]["type_achat"]));
      document.getElementsByClassName('grid-item')[i].appendChild(element);
    }

    for (var i = 0; i < articles.length; i++) {
      var image = document.createElement("img");
      var imageParent = document.getElementsByClassName("photo")[i];
      image.src = articles[i]["image1"];
      image.style = "width: 400px; height:190px;"
      imageParent.appendChild(image);
    }

    //case supprimer l'article
    for (var i = 0; i < articles.length; i++) {

      var suppr = document.createElement('input');
      suppr.setAttribute('indiceArticle',i);
      suppr.setAttribute('type', 'submit');
      suppr.setAttribute('value', 'Supprimer');

      console.log(suppr.getAttribute('indiceArticle', 'suppr' + i))

      suppr.classList.add("suppr");
      document.getElementsByClassName('grid-item')[i].appendChild(suppr);
    }
  </script>

  <!-- Fin liste article -->

  <footer>
    <div class="contenu">
      <p>
        ECE &copy; 2021 &nbsp;&nbsp;|&nbsp;&nbsp;
        <a href="#"> Mentions légales</a> &nbsp;&nbsp;|&nbsp;&nbsp;

        <a href="mailto:ecemarketplace@gmail.com"> Contactez-nous </a> &nbsp;&nbsp;|&nbsp;&nbsp;
        <a href="https://www.google.fr/maps/place/Rue+de+Mouza%C3%AFa,+75019+Paris/@48.8800816,2.395517,17z/data=!3m1!4b1!4m13!1m7!3m6!1s0x47e66dc8c1efd7e7:0x50b82c368941b70!2s19e+Arrondissement+de+Paris,+75019+Paris!3b1!8m2!3d48.8822253!4d2.3819534!3m4!1s0x47e66dbc06038bad:0x1c594ba784c8445b!8m2!3d48.8800816!4d2.3977057">
          Localisation</a>
      </p>
    </div>
  </footer>
  </div> <!-- fin englobe-->

  </body>

  </html>

<?php } else {
  header('Location: ventes.php');
}
?>