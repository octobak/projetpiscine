<?php
session_start();

$bdd = new PDO('mysql:host=localhost; dbname=marketplace;charset=utf8', 'root', '');

if (isset($_POST['envoie_message'])) {
  if (isset($_POST['destinataire'], $_POST['message']) && !empty($_POST['destinataire']) && !empty($_POST['message'])) {
    $destinataire = htmlspecialchars($_POST['destinataire']);
    $message = htmlspecialchars($_POST['message']);
    $PV = 1;
    $PA = 0;

    $id_destinataire = $bdd->prepare('SELECT CodeClient FROM client WHERE Nom = ?');
    $id_destinataire->execute(array($destinataire));
    $id_destinataire = $id_destinataire->fetch();
    $id_destinataire = $id_destinataire['CodeClient'];
    //expediteur = vendeur et destinataire = client
    $ins = $bdd->prepare('INSERT INTO messages (CodeVendeur,CodeClient,Mess,pv,pa) VALUES (?,?,?,?,?)');
    $ins->execute(array($_SESSION['CodeVendeur'], $id_destinataire, $message, $PV, $PA));

    $error = "Votre message a bien été envoyé";
  } else {
    $error = "compléter tous les champs";
  }
}

$destinataires = $bdd->query('SELECT Nom FROM client');

//<!-- PHP pour la partie envoie de message -->


//<!--  Debut Reception de message -->

//$db = new PDO('mysql:host=localhost; dbname=marketplace;charset=utf8', 'root', '');
$msg = $bdd->prepare('SELECT * FROM messages  WHERE CodeVendeur =? AND pv = 0 AND pa = 1 ');
$msg->execute(array($_SESSION['CodeVendeur']));
$msg_nbr = $msg->rowCount();
?>
<!-- reception de message -->

<!DOCTYPE html>
<html lang="fr">

<head>
  <meta charset="UTF-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <link rel="stylesheet" media="screen" href="../../css/notificationV.css">
  <link rel="preconnect" href="https://fonts.gstatic.com">
<link href="https://fonts.googleapis.com/css2?family=Mulish:wght@500&display=swap" rel="stylesheet">

  <title>Document</title>
</head>

<body>

  <div id="englobe">


    <nav class="navBar">
      <ul>

        <li><a class="active" href="#home">ECE VENDEUR<br>MARKETPLACE</a></li>

        <li>
          <div class="bloc">
            <a href="accueilV.php">
              <p>Accueil</p>
            </a>
          </div>
        </li>


        <li>
          <div class="bloc1">
            <a href="ventes.php"><span id="mes-ventes">Mes ventes</span></a>
          </div>
        </li>


        <li>
          <div class="bloc2">
            <a href="notificationV.php">Notifications</a>
          </div>
        </li>

        <li>
            <div class="idBonjour">
              <a> <span>Bonjour</span> <?php echo $_SESSION['Prenom'] ?></a>
            </div>
          </li>

        <li style="float:right" class="nav-item dropdown">
          <a href="#">
            <button class="cad">
              <img src="../../images/caddie.png" class="rounded-circle" height="40" alt="caddie" loading="lazy" />
            </button>
          </a>
        </li>

        <li style="float:right" class="nav-item dropdown">
          <div class="dropdown">
            <button class="dropbtn">
              <img src="../../images/profil.png" class="rounded-circle" height="40" alt="profil" loading="lazy" />
            </button>
            <div class="dropdown-content">
              <a href="profilV.php">Profil</a>
              <a href="../server/deconnexion.php">Déconnexion</a>
              <!-- <a href="#">Test</a> -->
            </div>
          </div>
        </li>

      </ul>

    </nav>

    <!-- Contenu de la page -->

    <div class="all">

      <!-- Debut Envoie de message -->
      <form method="POST">

        <label class="titre-bar">Destinataire : </label>
        <br>
        <select name="destinataire" class="select-bar">

          <?php while ($data = $destinataires->fetch()) { ?>

            <option> <?php echo $data['Nom'] ?> </option>

          <?php } ?>
        </select>

        <textarea placeholder="Votre message" name="message" cols="60" rows="5" class="zone-txt"></textarea>
        <br>
        <br>
        <div class="error-msg">
          <?php if (isset($error)) {
            echo $error;
          } ?>
        </div>
        <input type="submit" name="envoie_message" value="Envoyer" class="envoie-msg">
        <br><br>
      </form>
      <!-- Fin envoie de message -->

      <div class="all-messages">

        <!-- Début Affichage des msg -->
        <?php
        if ($msg_nbr == 0) {
          echo "vous n'avez pas de message";
        }
        while ($m = $msg->fetch()) {
          $PV = 0;
          $PA = 1;
          $exp_vendeur =  $bdd->prepare('SELECT Nom,pv,pa FROM client as c, messages as m WHERE c.CodeClient =? AND m.pv = 0 AND m.pa = 1');
          $exp_vendeur->execute(array($m['CodeClient']));
          $exp_vendeur = $exp_vendeur->fetch();
          $exp_vendeur = $exp_vendeur['Nom'];
        ?>
          <div class="bloc-msg">
            <b class="nom-exp"> <?= $exp_vendeur ?> </b> <span id="poli"> vous a envoyé: </span> <br>
            <span id="message"><?= $m['Mess'] ?></span>
          </div>

        <?php } ?>
      </div>
    </div>
    <!-- Fin Affichage des msg -->



    <!--Fin du contenu  -->

    <!-- footer -->
    <footer>
      <div class="contenu">
        <p>
          ECE &copy; 2021 &nbsp;&nbsp;|&nbsp;&nbsp;
          <a href="#"> Mentions légales </a> &nbsp;&nbsp;|&nbsp;&nbsp;

          <a href="mailto:ecemarketplace@gmail.com"> Contactez-nous </a> &nbsp;&nbsp;|&nbsp;&nbsp;
          
          <a href="https://www.google.fr/maps/place/Rue+de+Mouza%C3%AFa,+75019+Paris/@48.8800816,2.395517,17z/data=!3m1!4b1!4m13!1m7!3m6!1s0x47e66dc8c1efd7e7:0x50b82c368941b70!2s19e+Arrondissement+de+Paris,+75019+Paris!3b1!8m2!3d48.8822253!4d2.3819534!3m4!1s0x47e66dbc06038bad:0x1c594ba784c8445b!8m2!3d48.8800816!4d2.3977057">
            Localisation</a>
        </p>
      </div>
    </footer>
  </div> <!-- fin englobe-->


</body>

</html>